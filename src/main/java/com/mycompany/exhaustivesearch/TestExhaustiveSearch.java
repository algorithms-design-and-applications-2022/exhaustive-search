/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.exhaustivesearch;

/**
 *
 * @author Pinkz7_
 */
public class TestExhaustiveSearch {
    public static void main(String[] args) {
        int[] a = {1, 7, 2, 3, 11, 17, 13};
        showInput(a);
        ExhaustiveSearch exs = new ExhaustiveSearch(a);
        exs.process();
        exs.sum();

    }

    private static void showInput(int[] a) {
        System.out.print("Input is: ");
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
        System.out.println("");
        System.out.println("");
    }
}
